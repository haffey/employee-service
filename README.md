# Employee Service

This employee service implements the interview/hiring assignment given by Kenzan.

* [Building](#building)
* [Running](#running)
* [Notes](#notes)
* [Usage](#usage)
* [Contact](#contact)

## Building

This project requires Java 1.8, and is built using maven.  To build the project, run the following command.
```
mvn clean package
```

## Running

To run the server, use the maven spring-boot plugin:
```
mvn spring-boot:run
```

Alternatively, you can run the server without the plugin by using the created artifact directly:
```
java -jar [jar_path]
``` 

By default, the server will start on port 8080, and the employee database will be created as ./employees.db.  The port, and employee database location/filename can also be configured through the start up arguments as follows:
```
mvn spring-boot:run -Drun.arguments="--server.port=[port_to_run_on] --datastore.path=[path_to_employee_db_file] --datastore.filename=[filename_of_employee_db]"
```
or
```
java -jar [jar_path] --server.port=[port_to_run_on] --datastore.path=[path_to_employee_db_file] --datastore.filename=[filename_of_employee_db]
```

## Usage

_**Note:** The delete and update endpoints are different from other endpoints in that they support the use of **basic authentication**. This allows for a simplistic mechanism to restrict access to the delete endpoint as well as restrict ability to update the Status of an employee using the update endpoint. There is currently an in-memory user database with one entry that will allow you to test using these authenticated areas._

- Username: **admin**
- Password: **password**

### API Resources

- [GET /api/employees](#get-apiemployees)
- [GET /api/employees/[id]](#get-apiemployeesid)
- [POST /api/employees](#post-apiemployees)
- [PUT /api/employees/[id]](#put-apiemployeesid)
- [DELETE /api/employees/[id]](#delete-apiemployeesid)


#### GET /api/employees 

Example: http://localhost:8080/api/employees

Response Body: 
```
[
    {
        "ID": "e244141e-1230-425e-8db2-aa2dff1bf356",
        "FirstName": "Jane",
        "LastName": "Doe",
        "MiddleInitial": "X",
        "DateOfBirth": "1978-11-26T00:00:00.000+0000",
        "DateOfEmployment": "2013-01-18T00:00:00.000+0000",
        "Status": "ACTIVE"
    },
    {
        "ID": "0b3974eb-085b-4b77-9ede-83a03c4e64b9",
        "FirstName": "Bob",
        "LastName": "Kelso",
        "MiddleInitial": "F",
        "DateOfBirth": "1968-10-31T00:00:00.000+0000",
        "DateOfEmployment": "2012-03-18T00:00:00.000+0000",
        "Status": "ACTIVE"
    }
]
```

#### GET /api/employees/[id]

Example: http://localhost:8080/api/employees/e244141e-1230-425e-8db2-aa2dff1bf356"

Response body: 
```
{
    "ID": "e244141e-1230-425e-8db2-aa2dff1bf356",
    "FirstName": "Jane",
    "LastName": "Doe",
    "MiddleInitial": "X",
    "DateOfBirth": "1978-11-26T00:00:00.000+0000",
    "DateOfEmployment": "2013-01-18T00:00:00.000+0000",
    "Status": "ACTIVE"
}
```

#### POST /api/employees

Example: Create – POST http://localhost:8080/api/employees

Request body:
```
{
    "FirstName": "Jane",
    "LastName": "Doe",
    "MiddleInitial": "X",
    "DateOfBirth": "1978-11-26T00:00:00.000+0000",
    "DateOfEmployment": "2013-01-18T00:00:00.000+0000"
}
```
Response body:
```
{
    "ID": "e244141e-1230-425e-8db2-aa2dff1bf356",
    "FirstName": "Jane",
    "LastName": "Doe",
    "MiddleInitial": "X",
    "DateOfBirth": "1978-11-26T00:00:00.000+0000",
    "DateOfEmployment": "2013-01-18T00:00:00.000+0000",
    "Status": "ACTIVE"
}
```

#### PUT /api/employees/[id]

Example: Update – PUT http://localhost:8080/api/employees/e244141e-1230-425e-8db2-aa2dff1bf356

Request body:
```
{
    "FirstName": "June"
}
```
Response body:
```
{
    "ID": "e244141e-1230-425e-8db2-aa2dff1bf356",
    "FirstName": "June",
    "LastName": "Doe",
    "MiddleInitial": "X",
    "DateOfBirth": "1978-11-26T00:00:00.000+0000",
    "DateOfEmployment": "2013-01-18T00:00:00.000+0000",
    "Status": "INVACTIVE"
}
```

#### DELETE /api/employees/[id]

Example: Inactivate – DELETE http://localhost:8080/api/employees/e244141e-1230-425e-8db2-aa2dff1bf356

Response body:
```
{
    "ID": "e244141e-1230-425e-8db2-aa2dff1bf356",
    "FirstName": "June",
    "LastName": "Doe",
    "MiddleInitial": "X",
    "DateOfBirth": "1978-11-26T00:00:00.000+0000",
    "DateOfEmployment": "2013-01-18T00:00:00.000+0000",
    "Status": "INVACTIVE"
}
```

### Contact

Please feel free to reach out if you have any questions or issues with this service: [haffey.development@gmail.com](mailto:haffey.development@gmail.com)
