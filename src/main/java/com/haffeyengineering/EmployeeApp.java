package com.haffeyengineering;

import com.haffeyengineering.employeeservice.api.rest.EmployeeApi;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.haffeyengineering")
public class EmployeeApp {

    // -------------------
    // Fields
    // -------------------

    private static final Logger LOG = LogManager.getLogger(EmployeeApi.class);

    // -------------------
    // Entry point
    // -------------------

    public static void main(final String ... theArgs) {
        SpringApplication.run(EmployeeApp.class, theArgs);
        LOG.info("Employee Service is running!");
    }
}
