package com.haffeyengineering.employeeservice.db;

import com.haffeyengineering.employeeservice.models.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class FileDatastore {

    // -------------------
    // Fields
    // -------------------

    private static final Logger LOG = LogManager.getLogger(FileDatastore.class);
    private Map<String, Employee> data = new HashMap<>();

    @Value("${datastore.path}")
    private String datastorePath;
    @Value("${datastore.filename}")
    private String datastoreFilename;

    // -------------------
    // Constructors
    // -------------------

    public FileDatastore() {
    }

    // -------------------
    // Post Constructors
    // -------------------

    @PostConstruct
    private void initializeStore() {
        final File dbFile = new File(datastorePath, datastoreFilename);

        try (
                final ObjectInputStream ois = new ObjectInputStream(
                        new FileInputStream(dbFile))
        ) {
            data = (Map<String, Employee>) ois.readObject();
            LOG.info("Loaded employee data from {}", dbFile.getPath());
        }
        catch (final InvalidClassException | ClassNotFoundException ex) {
            LOG.fatal("Corrupted database file detected - ignoring data");
        }
        catch (final IOException ex) {
            LOG.warn("No initial database file detected.");
        }
    }

    // -------------------
    // Public Methods
    // -------------------

    public Employee getEmployee(final String theId) throws RuntimeException {
        return data.get(theId);
    }

    public Employee saveEmployee(final Employee theEmployee) throws RuntimeException {
        data.put(theEmployee.getId(), theEmployee);
        persistData();
        return theEmployee;
    }

    public Collection<Employee> getAllEmployees() throws RuntimeException {
        return data.values();
    }

    private void persistData() {
        try (
                final ObjectOutputStream oos = new ObjectOutputStream(
                        new FileOutputStream(new File(datastorePath, datastoreFilename)))
        ) {
            oos.writeObject(data);
        }
        catch (IOException theE) {
            theE.printStackTrace();
        }
    }
}
