package com.haffeyengineering.employeeservice.exceptions;

import org.springframework.http.HttpStatus;

public class EmployeeException extends RuntimeException {
    private HttpStatus response = HttpStatus.INTERNAL_SERVER_ERROR;

    // -------------------
    // Constructors
    // -------------------

    public EmployeeException() {
    }

    public EmployeeException(final String message, final HttpStatus theResponse) {
        super(message);
        response = theResponse;
    }

    // -------------------
    // Accessors
    // -------------------

    public HttpStatus getResponse() {
        return response;
    }

    // -------------------
    // Overrides
    // -------------------

    @Override
    public String getLocalizedMessage() {
        return String.format("employee_service_%s", getMessage().replaceAll("\\s", "_").toLowerCase());
    }

}
