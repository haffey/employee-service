package com.haffeyengineering.employeeservice.enums;

public enum EmploymentStatus {
    ACTIVE, INACTIVE
}
