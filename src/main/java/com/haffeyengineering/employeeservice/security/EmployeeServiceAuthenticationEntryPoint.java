package com.haffeyengineering.employeeservice.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.haffeyengineering.employeeservice.models.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class EmployeeServiceAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

    // -------------------
    // Fields
    // -------------------

    private final ObjectMapper mapper = new ObjectMapper();

    // -------------------
    // Overrides
    // -------------------

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authEx)
            throws IOException {
        response.addHeader("WWW-Authenticate", "Basic realm=\"" + getRealmName() + "\"");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        PrintWriter writer = response.getWriter();
        if ("application/json".equalsIgnoreCase(request.getContentType())) {
            response.setContentType(request.getHeader("Content-Type"));
            ErrorMessage em = new ErrorMessage(HttpStatus.FORBIDDEN.name(), "forbidden", "employee_service_forbidden");
            mapper.writeValue(writer, em);
        }
        else {
            writer.write("401 - Forbidden");
            response.setContentType(request.getHeader("text"));
        }

        writer.close();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setRealmName("Kenzan");
        super.afterPropertiesSet();
    }
}
