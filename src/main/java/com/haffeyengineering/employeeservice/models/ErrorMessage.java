package com.haffeyengineering.employeeservice.models;

import java.util.Date;

public class ErrorMessage {

    // -------------------
    // Fields
    // -------------------

    private final Date time = new Date();
    private final String response;
    private final String message;
    private final String localizedMessage;

    // -------------------
    // Contructors
    // -------------------

    public ErrorMessage(final String theResponse, final String theMessage, final String theLocalizedMessage) {
        response = theResponse;
        message = theMessage;
        localizedMessage = theLocalizedMessage;
    }

    // -------------------
    // Accessors
    // -------------------

    public Date getTime() {
        return time;
    }

    public String getResponse() {
        return response;
    }

    public String getMessage() {
        return message;
    }

    public String getLocalizedMessage() {
        return localizedMessage;
    }
}
