package com.haffeyengineering.employeeservice.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.haffeyengineering.employeeservice.enums.EmploymentStatus;

import java.io.Serializable;
import java.util.Date;

public class Employee implements Serializable {

    // -------------------
    // Fields
    // -------------------

    static final long serialVersionUID = 1L;

    @JsonProperty("ID")
    private String id;
    @JsonProperty("FirstName")
    private String firstName;
    @JsonProperty("LastName")
    private String lastName;
    @JsonProperty("MiddleInitial")
    private Character middleInitial;
    @JsonProperty("DateOfBirth")
    private Date dateOfBirth;
    @JsonProperty("DateOfEmployment")
    private Date dateOfEmployment;
    @JsonProperty("Status")
    private EmploymentStatus status = EmploymentStatus.ACTIVE;

    // -------------------
    // Accessors
    // -------------------

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Character getMiddleInitial() {
        return middleInitial;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public Date getDateOfEmployment() {
        return dateOfEmployment;
    }

    public EmploymentStatus getStatus() {
        return status;
    }

    // -------------------
    // Modifiers
    // -------------------

    public void setId(final String theId) {
        id = theId;
    }

    public void setFirstName(final String theFirstName) {
        firstName = theFirstName;
    }

    public void setLastName(final String theLastName) {
        lastName = theLastName;
    }

    public void setMiddleInitial(final Character theMiddleInitial) {
        middleInitial = theMiddleInitial;
    }

    public void setDateOfBirth(final Date theDateOfBirth) {
        dateOfBirth = theDateOfBirth;
    }

    public void setDateOfEmployment(final Date theDateOfEmployment) {
        dateOfEmployment = theDateOfEmployment;
    }

    public void setStatus(final EmploymentStatus theStatus) {
        status = theStatus;
    }

    // -------------------
    // Helpers
    // -------------------

    public void copyFromWithoutId(final Employee theFromEmployee, final boolean copyActiveStatus) {
        if(null != theFromEmployee.getFirstName()) {
            setFirstName(theFromEmployee.getFirstName());
        }
        if(null != theFromEmployee.getMiddleInitial()) {
            setMiddleInitial(theFromEmployee.getMiddleInitial());
        }
        if(null != theFromEmployee.getLastName()) {
            setLastName(theFromEmployee.getLastName());
        }
        if(null != theFromEmployee.getDateOfBirth()) {
            setDateOfBirth(theFromEmployee.getDateOfBirth());
        }
        if(null != theFromEmployee.getDateOfEmployment()) {
            setDateOfEmployment(theFromEmployee.getDateOfEmployment());
        }
        if(null != theFromEmployee.getStatus() && copyActiveStatus) {
            setStatus(theFromEmployee.getStatus());
        }
    }
}
