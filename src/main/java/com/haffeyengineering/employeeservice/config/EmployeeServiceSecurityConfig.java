package com.haffeyengineering.employeeservice.config;

import com.haffeyengineering.employeeservice.security.EmployeeServiceAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class EmployeeServiceSecurityConfig extends WebSecurityConfigurerAdapter {

    // -------------------
    // Fields
    // -------------------

    private final EmployeeServiceAuthenticationEntryPoint esaep;

    // -------------------
    // Contructors
    // -------------------

    @Autowired
    public EmployeeServiceSecurityConfig(final EmployeeServiceAuthenticationEntryPoint theEsaep) {
        esaep = theEsaep;
    }

    // -------------------
    // Overrides
    // -------------------

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.DELETE).hasRole("ADMIN")
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().httpBasic().authenticationEntryPoint(esaep);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("admin").password("password").roles("ADMIN");
        auth.inMemoryAuthentication().withUser("user").password("userpass").roles("USER");
    }

    // -------------------
    // Helpers
    // -------------------

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(final CharSequence rawPassword) {
                return rawPassword.toString();
            }

            @Override
            public boolean matches(final CharSequence rawPassword, final String encodedPassword) {
                return rawPassword.equals(encodedPassword);
            }
        };
    }
}
