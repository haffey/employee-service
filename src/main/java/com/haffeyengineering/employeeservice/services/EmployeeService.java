package com.haffeyengineering.employeeservice.services;

import com.haffeyengineering.employeeservice.db.FileDatastore;
import com.haffeyengineering.employeeservice.enums.EmploymentStatus;
import com.haffeyengineering.employeeservice.exceptions.EmployeeException;
import com.haffeyengineering.employeeservice.models.Employee;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    // -------------------
    // Fields
    // -------------------

    private static final int MAX_TRIES_FOR_UNIQUE_ID = 10;
    private final FileDatastore datastore;

    // -------------------
    // Constructors
    // -------------------

    @Autowired
    public EmployeeService(final FileDatastore theDatastore) {
        datastore = theDatastore;
    }

    // -------------------
    // Public methods
    // -------------------

    public Collection<Employee> getEmployees() {
        return datastore.getAllEmployees().stream().filter(e -> EmploymentStatus.ACTIVE == e.getStatus())
                .collect(Collectors.toList());
    }

    public Employee getEmployeeById(final String theId) {
        final Employee employee = getActiveEmployeeById(StringUtils.trimToNull(theId));

        if (null == employee) {
            throw new EmployeeException("employee not found", HttpStatus.NOT_FOUND);
        }

        return employee;
    }

    public Employee updateEmployee(final String theEmployeeId, final Employee theEmployee, boolean isAdminUser)
            throws EmployeeException {
        final Employee employee = datastore.getEmployee(StringUtils.trimToNull(theEmployeeId));
        if (null == employee) {
            throw new EmployeeException("employee not found", HttpStatus.NOT_FOUND);
        }

        employee.copyFromWithoutId(theEmployee, isAdminUser);
        datastore.saveEmployee(employee);

        return employee;
    }

    public Employee deleteEmployeeById(final String theId) {
        final Employee employee = getActiveEmployeeById(StringUtils.trimToNull(theId));
        if (null == employee) {
            throw new EmployeeException("employee not found", HttpStatus.NOT_FOUND);
        }

        employee.setStatus(EmploymentStatus.INACTIVE);
        datastore.saveEmployee(employee);
        return employee;
    }

    public Employee addEmployee(final Employee theEmployee) throws EmployeeException {
        int uniqueTries = 0;

        do {
            theEmployee.setId(UUID.randomUUID().toString());
            final Employee existing = datastore.getEmployee(theEmployee.getId());
            if (null == existing) {
                return datastore.saveEmployee(theEmployee);
            }

            uniqueTries++;

            if (uniqueTries >= MAX_TRIES_FOR_UNIQUE_ID) {
                throw new EmployeeException("could not create unique ID for new employee",
                        HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
        while (true);
    }

    // -------------------
    // Helpers
    // -------------------

    private Employee getActiveEmployeeById(final String theId) {
        final Employee employee = datastore.getEmployee(theId);
        if (null == employee || EmploymentStatus.INACTIVE == employee.getStatus()) {
            return null;
        }
        else {
            return employee;
        }
    }
}
