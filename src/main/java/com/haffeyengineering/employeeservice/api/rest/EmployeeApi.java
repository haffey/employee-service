package com.haffeyengineering.employeeservice.api.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.haffeyengineering.employeeservice.exceptions.EmployeeException;
import com.haffeyengineering.employeeservice.models.Employee;
import com.haffeyengineering.employeeservice.models.ErrorMessage;
import com.haffeyengineering.employeeservice.services.EmployeeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@ControllerAdvice
@RestController
public class EmployeeApi {

    // -------------------
    // Fields
    // -------------------

    private static final Logger LOG = LogManager.getLogger(EmployeeApi.class);
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private EmployeeService employeeService;

    // -------------------
    // Static setup
    // -------------------

    static {
        MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
    }

    // -------------------
    // Constructors
    // -------------------

    @Autowired
    EmployeeApi(final EmployeeService theEmployeeService) {
        employeeService = theEmployeeService;
    }

    // -------------------
    // Endpoints
    // -------------------

    @GetMapping("/employees")
    ResponseEntity<?> getEmployees() {
        LOG.debug("getEmployees called");

        final Collection<Employee> employees = employeeService.getEmployees();
        if (LOG.isTraceEnabled()) {
            LOG.trace(getFormattedJsonString(employees));
        }
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @GetMapping("/employees/{id}")
    ResponseEntity<?> getEmployeeById(@PathVariable("id") final String theEmployeeId) {
        LOG.debug("getEmployeeById called with employee ID '{}'", theEmployeeId);

        return new ResponseEntity<>(employeeService.getEmployeeById(theEmployeeId), HttpStatus.OK);
    }

    @PostMapping("/employees")
    ResponseEntity<?> addEmployee(@RequestBody final Employee theEmployee) {
        LOG.debug("addEmployee called");

        if (LOG.isTraceEnabled()) {
            LOG.trace(getFormattedJsonString(theEmployee));
        }

        final Employee employee = employeeService.addEmployee(theEmployee);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @DeleteMapping("/employees/{id}")
    ResponseEntity<?> deleteEmployeeById(@PathVariable("id") final String theEmployeeId) {
        LOG.debug("deleteEmployeeById called with employee ID '{}'", theEmployeeId);

        final Employee employee = employeeService.deleteEmployeeById(theEmployeeId);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @PutMapping("/employees/{id}")
    ResponseEntity<?> updateEmployeeById(@PathVariable("id") final String theEmployeeId,
            @RequestBody final Employee theChanges) {
        LOG.debug("updateEmployeeById called with employee ID '{}'", theEmployeeId);

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final boolean adminRole = auth.getAuthorities().stream().anyMatch(e -> "ROLE_ADMIN".equals(e.getAuthority()));
        final Employee updatedEmployee = employeeService.updateEmployee(theEmployeeId, theChanges, adminRole);
        return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);
    }

    // -------------------
    // Exception Handlers
    // -------------------

    @ExceptionHandler(value = { EmployeeException.class })
    ResponseEntity<?> handleException(final EmployeeException ex) {
        final ErrorMessage errorMessage = new ErrorMessage(ex.getResponse().name(), ex.getMessage(),
                ex.getLocalizedMessage());
        return new ResponseEntity<>(errorMessage, ex.getResponse());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    ResponseEntity<?> handleException(final HttpMessageNotReadableException ex) {
        final ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.name(),
                "invalid employee object - could not parse request body", "employee_service_invalid_employee_object");
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    // -------------------
    // Helpers
    // -------------------

    private static String getFormattedJsonString(final Object theObject) {
        try {
            return MAPPER.writeValueAsString(theObject);
        }
        catch (final Exception ex) {
            LOG.warn("Unable to create string representation of a JSON object - " + ex.getMessage(), ex);
            return theObject.toString();
        }
    }
}
